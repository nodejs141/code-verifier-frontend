import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import Editor from '../editor/Editor';
import { getKata } from '../services/katasService';
import { Kata } from '../utils/types/Kata.type';


interface KataDetailComponentProps {
    id: string
}

const KataDetailComponent = ({ id }: KataDetailComponentProps) => {

    const [kata, setKata] = useState<Kata | undefined>(undefined)
    const [showSolution, setShowSolution] = useState<boolean>(false)
    
    useEffect(() => {
        getKata(id)
            .then((res: AxiosResponse) => {
                if ( res.status === 200 && res.data ) {
                    setKata(res.data);
                } else {
                    throw new Error("Error in get all katas request");
                }
            })
            .catch((err) => console.error(`[GET KATAS BY ID]: ${err}`))
    }, [id])


    return (
        <div>
            { kata 
                ? <>
                    <h3>{kata?.name}</h3>
                    <p>{kata?.description}</p>
                    <p>Difficulty: {kata?.difficulty}</p>
                    <p>Creator: {kata?.creator.name}</p>
                    <p>Played: {kata?.played}</p>
                    <p>Stars: {Number(kata?.stars.value.$numberDecimal)}/5</p>

                    <button onClick={() => setShowSolution(!showSolution)}>
                        {showSolution ? 'Hide Solution' : 'Show Solution'}
                    </button>
                    { showSolution ? <Editor language='jsx'>{'ADD SOLUTION REQUEST'}</Editor> : null }
                </>
                : <h5>No Content</h5>
            }
        </div>
    )
}

export default KataDetailComponent;
