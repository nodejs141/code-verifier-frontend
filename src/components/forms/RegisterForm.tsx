import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { AxiosResponse } from 'axios';
import { register } from '../../services/authService';


const RegisterForm = () => {

    const initialValues = {
        name : '',
        email : '',
        password : '',
        confirm : '', // to confirm password
        age : ''
    }

    const registerSchema = Yup.object().shape(
        {
            name: Yup.string()
                .min(3, 'Name too short')
                .max(15, 'Name too long')
                .required('Username is required'),
            email: Yup.string()
                .email('Invalid email format')
                .required('Email is required'),
            password: Yup.string()
                .min(3, 'Password too short')
                .required('Password is required'),
            confirm: Yup.string()
                .when('password', {
                    is: (value: string) => (value && value.length > 0 ? true : false),
                    then: Yup.string().oneOf(
                        [Yup.ref('password')], '¡Password must match!'
                    )
                })
                .required('You must confirm the password'),
            age: Yup.number()
                .min(8, 'Less than the minimum age allowed')
                .max(90, 'Older than the maximum allowed age')
                .required('Age is required')
        }
    )


    return (
        <div>
            <h4>Register New User</h4>
            <Formik
                initialValues={ initialValues }
                validationSchema={ registerSchema }
                onSubmit={ async (values) => {
                    try {
                        const response: AxiosResponse = await register(values.name, values.email, values.password, Number(values.age));
                        if ( response.status === 201 && response.data ) {
                            alert(`Welcome ${values.name}`)
                        } else {
                            throw new Error("Invalid register");
                        }
                    } catch (err) {
                        console.error(`[REGISTER ERROR]: ${err}`)
                    }
                } }
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>
                            {/* Username field and Errors */}
                            <label htmlFor="name">Username</label>
                            <Field id="name" type="text" name="name" placeholder="Your username" />
                            { errors.name && touched.name && (
                                    <ErrorMessage name="name" component='div' />
                                )
                            }

                            {/* Email field and Errors */}
                            <label htmlFor="email">Email</label>
                            <Field id="email" type="email" name="email" placeholder="example@email.com" />
                            { errors.email && touched.email && (
                                    <ErrorMessage name="email" component='div' />
                                )
                            }

                            {/* Password field and Errors */}
                            <label htmlFor="password">Password</label>
                            <Field id="password" name="password" placeholder="password" type="password" />
                            { errors.password && touched.password && (
                                <ErrorMessage name="password" component='div' />
                                )
                            }

                            {/* Confirm Password field and Errors */}
                            <label htmlFor="confirm">Confirm</label>
                            <Field id="confirm" name="confirm" placeholder="Confirm password" type="password" />
                            { errors.confirm && touched.confirm && (
                                    <ErrorMessage name="confirm" component='div' />
                                )
                            }

                            {/* Age field and Errors */}
                            <label htmlFor="age">Age</label>
                            <Field id="age" name="age" placeholder="Your age" type="number" />
                            { errors.age && touched.age && (
                                    <ErrorMessage name="age" component='div' />
                                )
                            }

                            <button type="submit">Register Account</button>
                            { isSubmitting ? (<p>Sending data...</p>) : null }
                        </Form>
                    )
                }
            </Formik>
        </div>
    );
}

export default RegisterForm;
