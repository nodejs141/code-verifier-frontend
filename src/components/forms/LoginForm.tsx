import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { AxiosResponse } from 'axios';
import { login } from '../../services/authService';
import { useNavigate } from 'react-router-dom';


// Define Schema of validation with Yup
const loginSchema = Yup.object().shape(
    {
        email: Yup.string().email('Invalid Email Format').required('Email is required'),
        password: Yup.string().required('Password is required')
    }
);


// Login Component
const LoginForm = () => {

    const navigate = useNavigate();
    
    const initialCredentials = {
        email: '',
        password: ''
    }

    return (
        <div>
            <h4>Login Form</h4>
            <Formik 
                initialValues={ initialCredentials }
                validationSchema={ loginSchema }
                onSubmit={ async(values) => {
                    try {
                        const response: AxiosResponse = await login(values.email, values.password);
                        if ( response.status === 200 && response.data.token ) {
                            // Persist the JWT in sessionstorage, cookies or Redux
                            sessionStorage.setItem('sessionJWT', response.data.token);
                            navigate('/');
                        } else {
                            throw new Error("Invalid Credentials");
                        }
                    } catch (err) {
                        console.error(`[LOGIN ERROR]: ${err}`)
                    }
                } }
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>
                            {/* Email field and errors */}
                            <label htmlFor="email">Email</label>
                            <Field id='email' type='email' name='email' placeholder='example@email.com' />

                            { errors.email && touched.email && (
                                    <ErrorMessage name='email' component='div' />
                                )
                            }

                            {/* Password field and errors */}
                            <label htmlFor="password">Password</label>
                            <Field id='password' type='password' name='password' placeholder='example' />

                            { errors.password && touched.password && (
                                    <ErrorMessage name='password' component='div' />
                                )
                            }

                            {/* Submit Form */}
                            <button type='submit' >Login</button>

                            {/* Message if the form is submitting */}
                            { isSubmitting 
                                ? <p>Checking Credentials...</p> 
                                : null 
                            }
                        </Form>
                    )
                }
            </Formik>
        </div>
    )
}

export default LoginForm;
