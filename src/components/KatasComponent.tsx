import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAllKatas } from '../services/katasService';
import { Kata } from '../utils/types/Kata.type';


const KatasComponent = () => {
    
    const [totalPages, setTotalPages] = useState<number>(1)
    const [currentPage, setCurrentPage] = useState<number>(1)
    const [katasList, setKatasList] = useState<Kata[]>([])
    const navigate = useNavigate();
    
    useEffect(() => {
        getAllKatas(20, 1)
            .then((res: AxiosResponse) => {
                
                if ( res.status === 200 && res.data ) {
                    setTotalPages(res.data.totalPages);
                    setCurrentPage(res.data.currentPage);
                    setKatasList(res.data.katas);
                } else {
                    throw new Error("Error in get all katas request");
                }
            })
            .catch((err) => console.error(`[GET ALL KATAS ERROR]: ${err}`))
    }, [])
    

    /**
     * Method to navigate to Kata detail
     * @param id id of Kata
     */
    const goToKataDetail = (id: string) => {
        navigate(`/katas/${id}`)
    }

    return (
        <>
            { katasList.length > 0 
                ? katasList.map((kata: Kata) => {
                    return (
                        <div key={kata._id}>
                            <h3 onClick={() => goToKataDetail(kata._id)}>{kata.name}</h3>
                            <p>{kata.description}</p>
                            <p>Difficulty: {kata.difficulty}</p>
                            <p>Stars: {kata.stars.value.$numberDecimal}/5</p>
                        </div>
                    );
                })
                : <h5>No Content</h5>
            }
        </>
    )
}

export default KatasComponent;
