import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';

interface ProtectedRouteProps {
    children?: JSX.Element
    redirectTo?: string
}

const ProtectedRoute = ( { children, redirectTo='/login' }: ProtectedRouteProps ) => {
   
    const isLoggedIn = useSessionStorage('sessionJWT')
    if ( !!!isLoggedIn ) {
        return <Navigate to={redirectTo} />;
    }
    
    return children ? children : <Outlet/>
}

export default ProtectedRoute;
