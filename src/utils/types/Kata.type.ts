export type Kata = {
    _id: string,
    name: string,
    description: string,
    difficulty: string,
    played: number,
    creator: {
        name: string
    },
    stars: {
        value: {
            $numberDecimal: string
        },
        voted: number
    }
}
