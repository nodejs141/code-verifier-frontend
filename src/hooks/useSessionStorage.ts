export const useSessionStorage = (key: string): string | boolean => {
    const storageValue = sessionStorage.getItem(key);

    if ( !storageValue ) {
        return false;
    } else {
        return storageValue;
    }
}
