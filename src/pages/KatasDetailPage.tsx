import React from 'react';
import { Navigate, useParams } from 'react-router-dom';
import KataDetailComponent from '../components/KataDetailComponent';

const KatasDetailPage = () => {

    const { id } = useParams();
    if ( !!!id ) {
        return <Navigate to='/katas' />;
    }

    return (
        <div>
            <h1>Katas Detail Page</h1>
            <KataDetailComponent id={id} />
        </div>
    )
}

export default KatasDetailPage;
