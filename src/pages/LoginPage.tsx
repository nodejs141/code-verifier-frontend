import React from 'react';
import LoginForm from '../components/forms/LoginForm';


const LoginPage = () => {
    return (
        <div>
            <h1>LoginPage</h1>
            <LoginForm />
        </div>
    )
}

export default LoginPage;
