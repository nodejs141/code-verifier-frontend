import React from 'react';
import KatasComponent from '../components/KatasComponent';

const KatasPage = () => {
    
    return (
        <div>
            <h1>Katas Page</h1>
            <KatasComponent />
        </div>
    )
}

export default KatasPage;
