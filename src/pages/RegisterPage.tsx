import React from 'react'
import RegisterForm from '../components/forms/RegisterForm'

const RegisterPage = () => {
    return (
        <div>
            <h1>RegisterPage</h1>
            <RegisterForm />
        </div>
    )
}

export default RegisterPage;
