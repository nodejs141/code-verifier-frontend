import configAxios from '../utils/config/axios.config';


/**
 * Login Method
 * @param {string} email Email to login a user
 * @param {string} password Password to login a user
 * @returns 
 */
export const login = (email: string, password: string) => {

    // Create body to Post
    const body = {
        email,
        password
    }

    // Send Post request to login endpoint
    return configAxios.post('/auth/login', body) // http://localhost:8000/api/auth/login
}

/**
 * Register Method
 * @param {string} name Name of new user
 * @param {string} email Email of new user
 * @param {string} password Password of new user
 * @param {number} age Age of new user (min = 5 and max = 90)
 * @returns 
 */
export const register = (name: string, email: string, password: string, age: number) => {

    // Create body to Post
    const body = {
        name,
        email,
        password,
        age
    }

    // Send Post request to login endpoint
    return configAxios.post('/auth/register', body) // http://localhost:8000/api/auth/register
}
