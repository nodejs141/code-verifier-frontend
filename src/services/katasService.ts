import { AxiosRequestConfig } from 'axios';
import configAxios from '../utils/config/axios.config';


/**
 * GetAllKatas Method
 * @param {number} limit Maximum number of katas per page (Optional)
 * @param {number} page Page number to retrieve (Optional)
 * @param {string} sortKey Name of the field by which the information is to be organized (Optional)
 * @param {number} sortOrder Type of sort to execute (ascending = 1) or (descending = -1) (Optional)
 * @returns List with the designated katas
 */
export const getAllKatas = (limit?: number, page?: number, sortKey?: string, sortOrder?: 1 | -1) => {

    const options: AxiosRequestConfig = {
        headers: { 'x-access-token': sessionStorage.getItem('sessionJWT') },
        params: { limit, page, sortKey, sortOrder }
    }

    return configAxios.get('/katas', options) // http://localhost:8000/api/katas
}

/**
 * GetKata Method
 * @param {string} kataId id of the kata recover
 * @returns 
 */
export const getKata = (kataId: string) => {

    const options: AxiosRequestConfig = {
        headers: { 'x-access-token': sessionStorage.getItem('sessionJWT') },
        params: { kataId }
    }

    return configAxios.get('/katas', options) // http://localhost:8000/api/katas
}
