# Code-Verifier-Frontend


## Table of Contents

- [Get the Project](#get-the-project)
- [Project Dependencies](#project-dependencies)
- [Project Scripts](#project-scripts)
- [Getting Started](#getting-started)
- [Authors ](#authors )
- [License](#license)


## Get the Project

- [ ] [Go to the repository in gitlab](https://gitlab.com/nodejs141/code-verifier-frontend/-/tree/main) and review the project structure, you can download it the *.zip* or just use the command:

```
cd folder_location
git clone https://gitlab.com/nodejs141/code-verifier-frontend.git
```


## Project Dependencies

- ***`React:`*** It is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies. React can be used as a base in the development of single-page, mobile, or server-rendered applications with frameworks like *Nextjs*.

- ***`React-Dom:`*** It is a package provides DOM-specific methods that can be used at the top level of your app and as an escape hatch to get outside the React model if you need to

- ***`Babel:`*** It is a free and open-source JavaScript transcompiler that is mainly used to convert ECMAScript 2015+ code into backwards-compatible JavaScript code that can be run by older JavaScript engines.

- ***`Typescript:`*** It is a strict syntactical superset of JavaScript and adds optional static typing to the language. It is designed for the development of large applications and transpiles to JavaScript.

- ***`Eslint:`*** It is a static code analysis tool for identifying problematic patterns found in JavaScript code. Rules in ESLint are configurable, and customized rules can be defined and loaded. ESLint covers both code quality and coding style issues.

- ***`Jest:`*** It is a delightful JavaScript Testing Framework with a focus on simplicity and built on top of Jasmine.

- ***`React-Router-Dom:`*** It is an npm package that enables you to implement dynamic routing in a web app. It allows you to display pages and allow users to navigate them. It is a fully-featured client and server-side routing library for React. React Router Dom is used to build single-page applications i.e. applications that have many pages or components but the page is never refreshed instead the content is dynamically fetched based on the URL.

- ***`Axios:`*** It is a Javascript library used to make HTTP requests from node.js or XMLHttpRequests from the browser and it supports the Promise API that is native to JS ES6. It can be used intercept HTTP requests and responses and enables client-side protection against XSRF. It also has the ability to cancel requests..

- ***`Formik:`*** It is a small group of React components and hooks for building forms in React and React Native. It helps with the three most annoying parts: getting values in and out of form state, validation and error messages, handling form submission.

- ***`Yup`*** It is a JavaScript schema builder for value parsing and validation. Define a schema, transform a value to match, validate the shape of an existing value, or both. Yup schema are extremely expressive and allow modeling complex, interdependent validations or value transformations.

- ***`Material UI`*** It is an open-source React component library that implements Google's Material Design. It includes a comprehensive collection of prebuilt components that are ready for use in production right out of the box. Material UI is beautiful by design and features a suite of customization options that make it easy to implement your own custom design system on top of our components.

- ***`Serve:`*** It helps you serve a static site, single page application or just a static file (no matter if on your device or on the local network). It also provides a neat interface for listing the directory's contents.


## Project Scripts

- *"npm start":* Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits. You will also see any lint errors in the console.

- *"npm test":* Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

- *"npm run build":* Builds the app for production to the ***build*** folder. It correctly bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes. See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

- *"npm run eject":* If you aren’t satisfied with the build tool and configuration choices, you can ***eject*** at any time. This command will remove the single build dependency from your project.  
Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except ***eject*** will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.  
You don’t have to ever use ***eject***. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.  
**Note: this is a one-way operation. Once you ***eject***, you can’t go back!**


## Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Authors 
It is a personal project of S.U.


## License
No open source project.


## Project status

In progress...
